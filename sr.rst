==============================
SR.exe 阻止能と注入飛程の概算
==============================

SRIM には, 主要なソフトウェアとして,

SR.exe:
  阻止能と飛程の簡易計算(Stopping power and Range)
TRIM.exe:
  2体衝突シミュレーション(TRansport of Ions in Material)

の2つが含まれています. ここでは, SR.exe による阻止能と注入飛程の計算に
ついて説明します.

プログラムの起動
================

.. note::

   本文では, SRIM2013のパッケージを "C:\\users\\srim\\Documents\\srim2013" 
   にセットアップしているとします. 
   これを "SRIMセットアップフォルダ" または "%SRIM%" と呼ぶことにします.

SRIMセットアップフォルダを開きます. そのフォルダ内の"SRIM.exe" をダブルクリックし, プログラムを起動します.

.. image:: _static/sr/srim_start.png
   :width: 320px

この中の"Stopping/Range Tables"ボタンをクリックします. SR.exe プログラムに切り替わります.

.. note::

   SRIM.exe プログラムはSR.exe, TIN.exe (TRIM.exe のパラメータ編集プログラム)
   を起動するランチャーです. ですので, 直接 %SRIM%\\SR.exe をダブルクリック
   して起動してもかまいません.

.. image:: _static/sr/sr_input1.png
   :width: 320px

パラメータの設定
================

試しに, He(α粒子) の SiO2 への注入飛程の計算をしてみます.
エネルギーは 100eV から 10MeVのレンジとします.

パラメータは次のようになります.

   Ion(入射イオン) セクション:
     Symbol: **He**

     (Name: Helium)

     (Atomic Number: 2)

     (Mass: 4.003 (amu))

     Ion Energy Range Lowest: **0.1** (keV)
     
     Ion Energy Range Highest: **10000** (keV)

   Target(標的)セクション:
      Description : **He in SiO2** (これは自由に決められます)

      Density: **2.2** (g/cm\ :sup:`3`\ )

      Gas Tgt: **チェックを入れない**
               (固体と気体とでは平均自由行程や衝突係数の算出方法が異なります)

      Element 1:
        Symbol: **Si**

        (Name: Silicon)
        
        (Atomic Number: 14)

        (Mass: 28.086 (amu))

        Stoich: **1** (Stoichiometry, 構成比率)

      Element 2:
        Symbol: **O**
        
        (Name: Oxigen)

        (Atomic Number: 8)

        (Mass: 15.999 (amu))

        Stoich: **2**

   Stopping Power Units: **eV/Angstrom** (これは利用者のお好みで)

   Compound Correction: **1** (マジックパラメータです, 後述します)

これらの内, 括弧() で囲まれた部分は, 元素記号の入力や, 
周期表からの選択(黄色いPTボタンを押します)により自動的に入力されます.
また, 自動入力後に利用者が値を変更することもできます.

.. image:: _static/sr/sr_input2.png
   :width: 320px

計算の実施, 保存
================

パラメータが正しく入力されていることを確認できれば, 
"Calculate Table"ボタンをクリックします. 
この時, "結果をどこに, どのような名前で保存するか" 問い合わせがあります.

.. image:: _static/sr/sr_saveas.png
   :width: 320px

ここで表示されるパスは, SRIMセットアップフォルダからの相対位置になります.
上の例では,

  "C:\\users\\srim\\Documents\\srim2013\\SRIM Outputs\\Helium in Si-O2.txt"

として保存されます(途中のパスに空白が入りますが問題ないようです).

計算は即時に終了し, 結果が表示されます.

.. image:: _static/sr/sr_result1.png
   :width: 320px

内容はご覧になればすぐわかるかと思います. 
ファイルの前半部分は, 入力パラメータに関するまとめ, 後半は,

* Ion Energy: イオンのエネルギー (単位付き)
* dE/dx Elec.: 電子阻止能 (Stopping Unitsで指定された単位, 今回はeV/Ang.)
* dE/dx Nuclear: 核的阻止能 (同上)
* Projected range: 入射イオンの平均飛程(注入深さといってもいいです)
* Longitudinal Straggling: 飛程方向の標準偏差
* Radial Straggling: 動径方向の標準偏差

が並んでいます. これらはテキストファイルですので, 
excel等のツールでグラフ化することも可能です(エネルギーや距離に単位が付いているため, 扱いが面倒ですが). 

.. image:: _static/sr/He-SiO2-1.png
   :width: 320px

.. _sr__compound:

Component Dictionary と Compound correction
============================================

先ほどの例では, 標的材料の構成, 密度等を全てユーザーが入力していました.
SR.exe (及び後述するTIN.exe)では, 代表的な材料のパラメータを"Compount Dictionary"として提供しています.

先ほどの計算パラメータを一旦クリアして("Clear All"をクリック), "Compound Dictionary" をクリックします.

.. image:: _static/sr/sr_input3.png
   :width: 320px

様々な材料の構成元素比, 密度などがデータベースとして整理されています.
ここで, 星印の付いた材料があります. これらの材料は,
"Compound correction"の値が1以外に設定されている材料です. 
試しに水(Water)を選んでみます.

.. image:: _static/sr/sr_input4.png
   :width: 320px

この時, "Compound correction" の値が1.02となります. 
この値は, 電子阻止能に対する補正係数を表します. 
SR.exe での電子阻止能計算では, 最初それぞれの元素が独立して存在している物として個別に電子阻止能を計算し, 
それぞれの和をもって複合材料での電子阻止能とする, というモデルを採用しています. このモデルは, 入射イオンが高速な場合は妥当な近似なのですが, 低速イオンの衝突に対しては, 原子間の結合に寄与する電子の存在が無視できなくなります. 
SRIMの計算では, この低速領域における電子阻止能を多くの実験値に一致するよう
な補正係数をcompound correction(または Bragg correction)として Compound Dictionary に登録しています.

自由課題
========

1. 1MeV α線は空気中をどの程度進むでしょうか(Compound Table中にAirが登録されています). 
   また密度(=気圧)を変化させるとどうなるでしょうか
   
2. 皆さんが普段利用される材料のパラメータを思い出し, 阻止能, 注入飛程を算出してください。

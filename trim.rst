=================================
TRIM.exe 二体衝突シミュレーション
=================================

ここでは, SRIMの主な利用方法である2体衝突過程のシミュレーションの方法について
説明します. 慣例として2体衝突シミュレーションはTRIM (TRanspotation of Ion in Material) と呼ばれることもあります.

利用するプログラム, データの関係は以下の通りです.

TIN.exe:
  TRIM計算を行うパラメータファイルをセットアップするためのプログラムです.

TRIM.in:
  TRIM計算に必要なパラメータファイルです.

TRIM.exe:
  実際にTRIM計算を実施するプログラムです.

.. note::

   残念なことに, TRIMのシミュレーション条件を設定する TIN.exe は日本語Windowsの環境ではうまく動作しないことがあります. この場合, 以下の方法で利用できるようになります(原子力機構 杉本雅樹 様よりお教えいただきました. お礼申し上げます).

   1. 「デスクトップの表示」を行う. デスクトップ右下をクリックし, TIN.exeを含め, すべてのアプリケーションを最小化する .

   2. TIN.exe のアイコンをクリックし, これを復元する.

   3. (「デスクトップの表示」と復元は「Windows+d」のショートカットでも実施できる. したがって「Windows+d を2回押す」でも同様の効果が得られる)

TIN.exe の起動
==============

TIN.exe はTRIM計算パラメータセットアッププログラムです. 起動するには.
"SRIM.exe"をダブルクリックし, この中の"TRIM Calculation"をクリックします.
または, 直接 TIN.exe をダブルクリックして起動してもかまいません.

.. image:: _static/trim/trim_input1.png
   :width: 320px

パラメータのセットアップ
========================

SR.exe と同様に入射イオン, ターゲットの物性値を入力します. 
ターゲットには多層膜構造を適用することもできます.

ここでは, SiO2/Si 膜に対し 20keV Cs を垂直衝突させる系を考えてみましょう.

材料の指定
----------

次のようにパラメータをセットします.

Ion Data:
  Symbol: **Cs** (Atomic number=55, Mass=132.905 は自動的に挿入されます)

  Energy (keV): **20**

  Angle of Incidence: **0** (垂直入射)

Target Data:
   Layer 1:
     Layer Name: **SiO2**  (名前は任意です)

     Width: **100 Ang.**

     Density (g/cm3): **2.32**

     Compount Corr.: **1** (:ref:`sr__compound` を参照)

     Element 1:
       Symbol: **O**

       Atom Stoich.: **2**

       Damage (Disp, Latt, Surf) (eV): **28, 3, 2** (:ref:`physics__knockon` を参照)
     Element 2:
       Symbol: **Si**

       Atom Stoich.: **1**

       Damage (Disp, Latt, Surf) (eV): **15, 2, 4.7**

   Layer 2:
     Layer Name: **Silicon**  (名前は任意です)

     Width: **900 Ang.**

     Density (g/cm3): **2.32**

     Compount Corr.: **1**

     Element 1:
       Symbol: **Si**

       Atom Stoich.: **1**

       Damage (Disp, Latt, Surf) (eV): **15, 2, 4.7**

計算手法, その他の指定
----------------------------------

材料の指定に続き, 計算手法, データの見せ方などを指定します.

計算手法
^^^^^^^^

"Type of TRIM Calculation" の **DAMAGE** の部分が "Detailed Calculation with Full Cascade Damage" となっていることを確認します. この計算では, 高エネルギーでもってはじき出された標的原子が2次的, 3次的なはじき出しを生じることを考慮します. 通常はこの計算モデルを採用するのが妥当でしょう.

他のモデルとしては, 

- Ion Distribution and Quick Calculation of Damage: 注入イオンの運動ついてのみ2体衝突モデルを適用し, はじき出された標的原子によるカスケード生成を無視します. 
- Monolaer Collision Steps / Surface Sputtering: 
  スパッタ率を正確, かつできる限り高速に求めるため, 表面近傍のみカスケード計算を実施します. 

が有ります.

画面表示
^^^^^^^^

**Basic Plots** は, シミュレーション中に表示するイオン、はじき出し原子の軌跡の
表示方法を選択できます. 
X軸が深さ方向になります. デフォルトの"Ion Distribution with Recoils projected on Y-Plane" はXY平面のプロットを表示することとなります. これ以外に

- XZ平面
- XY平面. ただし標的原子はじき出しは表示しない
- YZ平面
- 上記4つ全て(含む跳ね返り原子のXYプロット)を統合したプロット
- 何も表示しない

計算速度の点からいえば、プロットを表示しない方が, はるかに高速に計算が進みます. 今回の例では, 全て表示にしておきましょう(All Four of the above on one screen)

画面下に **Plotting Window Depth** という項目が有ります. 
これは, 前述のプロットや, 後で説明する分布グラフにおいて表示する範囲を指定します. 只今のシミュレーションでは, 総合膜厚が1000Åなので, 1から1000を指定しておきます(プロットの見栄えが気になる場合は, この範囲を適宜調整します).

試行回数と乱数の種
^^^^^^^^^^^^^^^^^^

イオンの注入レンジ, ダメージの生成率, etc. といったイオン衝突により
引き起こされる現象は, 

- 標的材料内のどの元素と衝突するか?

- 衝突時の衝突係数(インパクトパラメータ)は幾つか?

といったパラメータにより大きく異なると予想されます. 従って, 
多数の条件による試行を通じて上記の現象の平均的な挙動を知る必要が有ります.
**Total Number of Ions** でこの試行回数を設定します. まずは, 
1回だけにします.

一方, TRIMの計算において, 上記2体衝突に関するパラメータはランダムに
決定されます. **Random Number Seed** は, 乱数生成の初期値を指定します(デフォルトは0). この値を指定することで, 必ず同じ条件でシミュレーションが再現できることを保証します.

出力ファイル
^^^^^^^^^^^^

**Output Disk Files** の部分は, シミュレーション途中で生成されるデータの内
保存する対象を指定します. ここで指定するファイルは, 試行回数の増加によって
容量が肥大する可能性が有りますので, 利用には注意が必要です.

各オプションに対するファイル名とその内容は以下の通りです.

Ion Ranges ("%SRIM%\\SRIM Output\\Range_3D.txt"):
  入射イオンの最終位置情報. 試行回数と同じ数のレコードが生成されます.

Backscattered Ions ("%SRIM%\\SRIM Output\\BACKSCAT.txt"):
  標的材料表面から脱離した入射イオンの情報

Transmitted Ions ("%SRIM%\\SRIM Output\\TRANSMIT.txt"):
  標的材料裏面から脱離した入射イオンの情報, オプションを指定することで, 標的原子の情報も保存されます.

Sputtered Atoms ("%SRIM%\\SRIM Output\\SPUTTER.txt"):
  標的材料表面から脱離した標的材料原子の情報

Collision Details ("%SRIM%\\SRIM Output\\COLLISION.txt"):
  全ての衝突イベントの記録. 容量が膨大になるので, 通常は指定しない方が良いでしょう.

今回は, "Sputtered Atoms" だけを指定します.

.. image:: _static/trim/trim_input2.png
   :width: 320px

単一照射の実行とデータの保存
============================

パラメータの入力が終われば, "Save Input & Run TRIM"のボタンをクリックします.
TRIM.exe プログラムが起動し, TRIMのシミュレーションが開始されます. 
現在, "Total Number of Ion” を1 に設定しているので, 
1回照射シミュレーションを実行して終了します.

終了時に"Calculate Complete (1 ions) ..."のダイアログボックスが出てきます.
試行回数が不十分で有ると思った場合は, さらに増やすことができますが, 
ここでは, "Yes = Save Data" を選びます.

.. image:: _static/trim/trim_endcalc.png
   :width: 320px

Save Data画面が表示されます. "Store SRIM directory (default)" をクリックします.

.. image:: _static/trim/trim_save.png
   :width: 320px

ここで保存する情報について, 次の2点が重要になります.

1. "Store SRIM directory (default)" をクリックしてデータを保存した場合,
   "%SRIM%\\SRIM Restore" フォルダにシミュレーションの途中経過
   全ての情報が, \*.sav または \*.bmp として保存されます.

2. この保存データは, パラメータ入力画面において"Resume saved TRIM calc."
   ボタンを押すことで, 呼び出し, 再開できます.

シミュレーション画面の説明です. シミュレーション結果に関係する部分のみ
取り上げます.

.. image:: _static/trim/trim_exec1.png
   :width: 320px

画面中央の軌跡データは, "COLLISION PLOTS" の項目で表示, 非表示が切り替えられます. 点の色がそれぞれ原子が移動した軌跡, または最終位置に相当します. 
入射イオンの軌跡から枝分かれするように, 標的原子に含まれるO, Siがはじき出され,
更なる衝突カスケードが生成される様子が観察されます.

他の衝突パターンを観察するならば, パラメータ入力プログラム(TIN.exe)において, 
Random number seed に異なる値を入力してみましょう. 
(ついでに Plotting Window Depths を 0~500 と狭めた方がわかりやすいでしょう)

繰り返し衝突と解析できるデータ
==============================

それでは, 何回も衝突シミュレーションを繰り返すことで, イオン注入レンジ, 
スパッタ率がどのように変化するのか, 調べていきます.

TIN.exe において, Total Number of Ions を1000にセットし, 
"Save Input & Run TRIM" をクリックし, シミュレーションを開始します.

.. image:: _static/trim/trim_exec2.png
   :width: 320px

統計量
------

イオンの注入レンジ, スパッタ率等は画面右の"Calculation Parameters"
の部分に表示されます. これらの値が試行毎に変化していること, 
また, 試行回数の増加に伴い, ある値に収束していく様子が観察されるでしょう.
算出される値には次のようなものが有ります.

Vacancies/Ion:
  1回のイオン衝突により生成される欠陥数.

Ion longitudinal Range, struggle:
  イオンの最終位置の深さ方向(X軸)の平均値, その標準偏差です

Ion radial Range, struggle:
  イオンの最終位置の動径方向(=(y^2+z^2)^0.5)の平均値, その標準偏差です

Energy loss:
  照射イオンのエネルギーがどのように消費されたかを示します.

    Ionization: 電子阻止能によるもの

    Vacancies: 標的原子のはじき出し

    Phonons: 標的材料の格子振動

  さらに, エネルギーを与えた粒子が, 1次入射イオンなのか, 
  はじき出しを受けた標的原子なのかに分類しています.

Sputtering Yeld:
  スパッタ率です, 標的元素ごとのスパッタ率, 
  脱離時の平均エネルギーが算出されます.
  
度数分布情報
------------

一方, 画面右の"DISTRIBUSIONS"プロットでは, 照射イオンの分布, ダメージの内訳など, 度数分布で表現できる量がグラフとして表示できます. 
表示するためには該当する項目の"Plot"列にチェックを入れます.
また, "F"ボタンを押すと数値をテキストデータとして保存できます
(デフォルトでは,"%SRIM%\\SRIM Output"に保存しようとします). 
幾つかの例について説明します.

Ion / Recoil distributions:
  入射イオン, はじき出し原子の深さ分布です.

  .. image:: _static/trim/trim_ion_distributions.png
    :width: 320px

  .. image:: _static/trim/trim_recoil_distributions.png
    :width: 320px

Damage Events:
  標的原子のはじき出しが生じた位置分布です. 衝突原子がそのほとんどのエネルギーを
  被衝突原子の与えて, その格子位置に静止した場合, "Replacement Collision"と判定されます.

  したがいまして,

    (Target Displacements) = (Target Vacancies) + (Replacement Collisions)

  の関係にあります.

  .. image:: _static/trim/trim_collision_events.png
    :width: 320px
  
Intengral / Differential sputtered ions(atoms?):
  スパッタされた標的原子の運動エネルギー分布です.
  運動エネルギー鉛直成分が, Surface Binding Energy (SBE) よりも大きい場合,
  その粒子はスパッタされたものと判定されます. 今回のシミュレーションでは,
  O:2eV, Si:4.7eV となっています.
  Integral sputterd ions のグラフ中で O:2eV, Si:4.7eV となる点が, スパッタ率(O:3.73, Si:1.03)
  を与えます(グラフ中の3.8eVは, (O:2eV + Si:4.7eV + Si:4.7eV)/3 として算出されているようです).

  .. image:: _static/trim/trim_sputter_int.png
    :width: 320px

  .. image:: _static/trim/trim_sputter_diff.png
    :width: 320px

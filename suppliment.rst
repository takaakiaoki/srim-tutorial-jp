======
資料
======

セミナー資料
============

`SRIM-tutorial.pptx <_static/SRIM-tutorial.pptx>`_

  `学術振興会マイクロビームアナリシス第141委員会`_\ 研修セミナー
  (2013/04/18-19)で使用したパワーポイントファイルです

.. _学術振興会マイクロビームアナリシス第141委員会: http://www.jsps.go.jp/j-soc/list/141.html

.. _SUZU_label:

SUZU
====

`SUZU <https://github.com/takaakiaoki/suzu/wiki>`_

  SUZU https://github.com/takaakiaoki/suzu/wiki

  TIN.exe に代わり日本語windows環境でもTRIMのパラメータファイル(TRIM.in) 
  を作成, TRIMを実行する為のプログラムです.

.. SRIM Tutorial master file, created by
   sphinx-quickstart on Fri Mar 08 10:22:39 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SRIM Tutorial
==================

Contents:

.. toctree::
   :maxdepth: 2

   introduction
   physics
   sr
   trim
   setup
   suppliment


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

